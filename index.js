var player_a = '';
var player_b = '';
var instructions = document.getElementById('instructions');
var startFlag = false;
var playerTurn = 'player_a';
var arrPlayerA = [];
var arrPlayerB = [];
var isGameFinish = false;
var winnerPlayer = '';
var sizeOfBoard = 0;
function coba(idBlock) {
    if(!startFlag) {
        alert('Please start the game')
        return false;
    } 

    if(isGameFinish) {
        if (winnerPlayer !== '') {
            winnerPlayer = player_a
        }
        alert(winnerPlayer + ' is the winner');
        return false;
    }

    var id = idBlock;
    var numberId = idBlock.split('-')[1];
    var value = document.getElementById(id).innerHTML;
    if(value !== '') {
        alert('place in a blank space')
        return false;
    }

    if(playerTurn === 'player_a') {
        document.getElementById(id).innerHTML = 'X';
        document.getElementById(id).style.color = "red";
        arrPlayerA.push(parseInt(numberId));
        if(checkIsPlayerWin(arrPlayerA)) {
            winnerPlayer = player_a
            alert(player_a+' is the winner');
            return false;
        }

        playerTurn = 'player_b';
        instructions.innerHTML = player_b + ' turns';
    } else if (playerTurn === 'player_b') {
        document.getElementById(id).innerHTML = 'O';
        document.getElementById(id).style.color = "blue";
        arrPlayerB.push(parseInt(numberId));
        if (checkIsPlayerWin(arrPlayerB)) {
            winnerPlayer = player_b;
            alert(player_b + ' is the winner');
            return false;
        }

        playerTurn = 'player_a';
        instructions.innerHTML = player_a + ' turns';
    }

    var allBlock = document.getElementsByClassName('fill');
    var temp = 0;
    for (var x = 0; x < allBlock.length; x++) {
        if (allBlock[x].innerHTML !== '') {
            temp++;
        }
    }
    if (temp === allBlock.length ) {
        alert('Please refresh the page. Previous game is over. no one wins.');
        return false;
    }
}

function setName(player, name) {
    if(player === 'player_a') {
        player_a = name;
    } else {
        player_b = name;
    }
}

function startGame() {
    var input = document.getElementsByTagName('input');
    for (var y = 0; y < input.length; y++) {
        input[y].disabled = 'disabled';
    }

    instructions.innerHTML = player_a+' turns';
    startFlag = true;
}

function checkIsPlayerWin(arrayPlayer) {
    arrayPlayer.sort();
    if(arrayPlayer.length < sizeOfBoard) {
        return false;
    }

    for(var i = 0; i < arrayPlayer.length; i++) {
        var temp = arrayPlayer[i]%sizeOfBoard;
        var sementara = arrayPlayer[i];
        var tempCtr = 0;
        var penambah = 0;
        if(temp === 1) {
            penambah = 1;
        } else {
            penambah = sizeOfBoard;
        }

        for (var x = 0; x < sizeOfBoard; x++) {
            sementara = parseInt(sementara) + parseInt(penambah);
            if (arrayPlayer.indexOf(parseInt(sementara)) > -1) {
                tempCtr++;
            }
        }

        if (tempCtr === (sizeOfBoard - 1)) {
            isGameFinish = true;
            return true;
        }

        if(arrayPlayer[i] === 1) {
            tempCtr = 0;
            var sementara = arrayPlayer[i];
            penambah = sizeOfBoard;
            for (var x = 0; x < sizeOfBoard; x++) {
                sementara = parseInt(sementara) + parseInt(penambah);
                if (arrayPlayer.indexOf(parseInt(sementara)) > -1) {
                    tempCtr++;
                }
            }

            if (tempCtr === (sizeOfBoard - 1)) {
                isGameFinish = true;
                return true;
            }
        }

        if (arrayPlayer[i] === 1) {
            tempCtr = 0;
            var sementara = arrayPlayer[i];
            penambah = parseInt(sizeOfBoard) + 1;
            for (var x = 0; x < sizeOfBoard; x++) {
                sementara = parseInt(sementara) + parseInt(penambah);
                if (arrayPlayer.indexOf(parseInt(sementara)) > -1) {
                    tempCtr++;
                }
            }

            if (tempCtr === (sizeOfBoard - 1)) {
                isGameFinish = true;
                return true;
            }
        }

        if (arrayPlayer[i] === parseInt(sizeOfBoard)) {
            tempCtr = 0;
            var sementara = arrayPlayer[i];
            penambah = parseInt(sizeOfBoard) - 1;
            for (var x = 0; x < sizeOfBoard; x++) {
                sementara = parseInt(sementara) + parseInt(penambah);
                if (arrayPlayer.indexOf(parseInt(sementara)) > -1) {
                    tempCtr++;
                }
            }

            if (tempCtr === (sizeOfBoard - 1)) {
                isGameFinish = true;
                return true;
            }
        }
    }
    return false;
}


function setBoard(size) {
    sizeOfBoard = size;
    if(parseInt(size) < 2) {
        alert("minimum size of board is 2");
        return false;
    }
    var ctr = 1;
    var string = '';
    for(var i = 0; i < size; i++) {
        string += '<tr>';
        for(var j = 0; j < size; j++) {
            string += "<td id='block-"+(ctr++)+"' class='fill' onclick='coba(this.id)'></td>";
        }
        string += '</tr>';
    }
    document.getElementById('tictactoe-board').innerHTML = string;
}